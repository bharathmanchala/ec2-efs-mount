terraform { 
  required providers { 
    aws = { 
      source = "hashicorp/aws" 
      #version = "~> 3.21" # Optional but recommended in production 
    } 
  } 
} 
#-------  Provider Information  ------------
provider "aws" {
  version = "~> 3.0"
  region  = var.region
}